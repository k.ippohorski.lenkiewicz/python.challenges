# Zadanka 12.2

# 12.2-1

from pathlib import Path

file_path = Path.home() / 'my_folder' / 'my_file.txt'
print(file_path)

# 12.2-2

print(file_path.exists())

# 12.2-3

print(file_path.name)

# 12.2-4

print(file_path.parent.name)
