# Zadanka 16.2

# 16.2-1

from urllib.request import urlopen

url = 'http://olympus.realpython.org/profiles'
page = urlopen(url)
html = page.read().decode('utf-8')

# 16.2-2

from bs4 import BeautifulSoup as bs

soup = bs(html, 'html.parser')
links = soup.find_all('a')
links = [link['href'] for link in links]

# 16.2-3

# slicing the original link for generating full addresses for each of the links
# tedious task, could be achieved more easly using urljoin from urllib.parse
# but let's do it anyway ;)
slice_index = url.find('/profiles')
url_short = url[:slice_index]

for link in links:
    new_url = url_short + link
    temp_page = urlopen(new_url)
    new_html = temp_page.read().decode('utf-8')
    new_soup = bs(new_html, 'html.parser')
    print(new_soup.get_text())
