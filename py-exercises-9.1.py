# Zadanka 9.1

# 9.1-1

cardinal_members = ('first', 'second', 'third')

# 9.1-2

print(cardinal_members[1])

# 9.1-3

position1, position2, position3 = cardinal_members

print(position1)
print(position2)
print(position3)

# 9.1-4

my_name = tuple('Konrad')

# 9.1-5

print('x' in my_name)

# 9.1-6

new_tuple = my_name[1:]
print(new_tuple)
