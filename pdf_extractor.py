
import easygui as gui
from pypdf import PdfReader, PdfWriter
# using PyPDF 3.17.4

# input
input_path = gui.fileopenbox(
    title='Select a PDF to extract from...',
    default='*.pdf'
)

if input_path is None:
    exit()

# get which pages
input_file = PdfReader(input_path)
input_pdf_page_count = len(input_file.pages)

start_index = gui.integerbox(
    title='PDF extractor',
    msg='Enter starting page number:',
    lowerbound=1,
    upperbound=input_pdf_page_count
)

if start_index is None:
    exit()

end_index = gui.integerbox(
    title='PDF extractor',
    msg='Enter ending page number:',
    lowerbound=start_index,
    upperbound=input_pdf_page_count
)

if end_index is None:
    exit()

# get save dir
save_title = 'Save the extracted PDF as...'
file_type = '*.pdf'
output_path = gui.filesavebox(title=save_title, default=file_type)

while input_path == output_path:
    gui.msgbox(msg='Cannot overwrite the original file!')
    output_path = gui.filesavebox(title=save_title, default=file_type)

if output_path is None:
    exit()

# perform extraction
output_pdf = PdfWriter()

for i in range(start_index, end_index+1):
    output_pdf.add_page(input_file.pages[i-1])

with open(output_path, 'wb') as output_file:
    output_pdf.write(output_file)

if start_index == end_index:
    output_msg = f'Page {start_index}'
else:
    output_msg = f'Pages {start_index}-{end_index}'

gui.msgbox(
    msg=f'{output_msg} successfully extracted!'
)
