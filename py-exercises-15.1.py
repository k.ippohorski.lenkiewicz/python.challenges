# zadanka 15.1

# 15.1-1

import sqlite3

initial_query = """
DROP TABLE IF EXISTS Roster;
CREATE TABLE Roster(
    Name TEXT,
    Species TEXT,
    Age INT
);"""

with sqlite3.connect('test_database2.db') as connection:
    cursor = connection.cursor()
    cursor.executescript(initial_query)

# 15.1-2

data_to_populate = (
    ('Benjamin Sisko', 'Human', 40),
    ('Jadzia Dax', 'Trill', 300),
    ('Kira Nerys', 'Bajoran', 29)
)

insert_query = "INSERT INTO Roster VALUES(?, ?, ?);"

with sqlite3.connect('test_database2.db') as connection:
    cursor = connection.cursor()
    cursor.executemany(insert_query, data_to_populate)

# 15.1-3

update_query = "UPDATE Roster SET Name='Ezri Dax' WHERE Name='Jadzia Dax';"

with sqlite3.connect('test_database2.db') as connection:
    cursor = connection.cursor()
    cursor.execute(update_query)

# 15.1-4

with sqlite3.connect('test_database2.db') as connection:
    cursor = connection.cursor()
    cursor.execute("SELECT Name, Age FROM Roster WHERE Species == 'Bajoran'")
    for row in cursor.fetchall():
        print(row)
