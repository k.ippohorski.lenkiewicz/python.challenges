# Zadanka 10.2

# 10.2-1

class Dog:
    species = 'Canis familiaris'
    
    def __init__(self, name, age, coat_color):
        self.name = name
        self.age = age
        self.coat_color = coat_color

    def __str__(self):
        return f'{self.name} is {self.age} years old'

    def speak(self, sound):
        return f'{self.name} says {sound}'

philo = Dog('Philo', 5, 'brown')
print(f'{philo.name}\'s coat is {philo.coat_color}.')

# 10.2-2

class Car:
    def __init__(self, color, mileage):
        self.color = color
        self.mileage = mileage

    def __str__(self):
        return f'The {self.color.lower()} car has {self.mileage:,} miles.'

    def drive(self, distance):
        self.mileage += distance

red = Car('red', 30_000)
blue = Car('blue', 20_000)

print(blue)
print(red)

# 10.2-3

yellow = Car('yellow', 0)
yellow.drive(100)
print(yellow)

