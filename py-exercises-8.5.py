# Zadanka 8.5

# 8.5-1

user_text = input('Please let me out. Type something: ')

while True:
    for character in user_text.lower():
        if character == 'q':
            break
    else:
        user_text = input('That didn\'t work. Try again: ')
        continue
    break

print('I\'m out, thanks!')

# @Daniel, czy to rozwiązanie spełnia przechodzi twoje code review?

# 8.5-2

for i in range(1, 51):
    if i % 3 == 0:
        continue
    print(i)
