# Zadanka 9.3

# 9.3-1

data = ((1, 2), (3, 4))

# 9.3-2

for i in range(len(data)):
    print(f'Row {i + 1} sum: {sum(data[i])}')

# 9.3-3

numbers = [4, 3, 2, 1]

# 9.3-4

numbers2 = numbers[:]

# 9.3-5

numbers.sort()

print(numbers)
print(numbers2)
