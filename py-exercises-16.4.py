import mechanicalsoup
import time

browser = mechanicalsoup.Browser()

for i in range(4):
    page = browser.get('http://olympus.realpython.org/dice')
    result_tag = page.soup.select('#result')[0]
    time_tag = page.soup.select('#time')[0]
    result = result_tag.text
    clock = time_tag.text[-10:]
    print(f'result: {result} @ {clock}')
    if i < 3:
        time.sleep(2)
