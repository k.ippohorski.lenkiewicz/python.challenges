# Zadanka 12.5

# 12.5-1

from pathlib import Path

lines = [
    
    'Discovery\n',
    'Enterprise\n',
    'Defiant\n',
    'Voyager\n',
    
]

with (Path.home() / 'starships.txt').open(mode='w', encoding='utf-8') as file:
    file.writelines(lines)
    
# 12.5-2

with (Path.home() / 'starships.txt').open(mode='r', encoding='utf-8') as file:
    for line in file.readlines():
        print(line, end='')

# 12.5-3

with (Path.home() / 'starships.txt').open(mode='r', encoding='utf-8') as file:
    for line in file.readlines():
        if line[0] == 'D':
            print(line, end='')
