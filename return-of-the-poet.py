import tkinter as tk
import random
from tkinter.filedialog import asksaveasfilename


def a_or_an(word, is_capital):
    an_letters = ('a', 'e', 'i', 'o', 'u')
    word_lowered = word.lower()
    determiner = 'a'
    if is_capital:
        determiner = 'A'
    if word_lowered[0] in an_letters:
        return f'{determiner}n {word}'
    else:
        return f'{determiner} {word}'


def extract_words(entry, min_words_count):
    words = entry.get().split(',')
    if len(words) < min_words_count:
        return False
    words_clean = []
    for word in words:
        word_clean = word.strip()
        words_clean.append(word_clean)
    return random.sample(words_clean, min_words_count)


def poem_gen():
    # setup
    noun = extract_words(ent_nouns, 3)
    verb = extract_words(ent_verbs, 3)
    adj = extract_words(ent_adjectives, 3)
    prep = extract_words(ent_prepositions, 3)
    adv = extract_words(ent_adverbs, 1)

    # check for correct input
    if any(word_list is False for word_list in (
        noun, verb, adj, prep, adv
    )):
        lbl_poem['text'] = ('Not enough input words.\n'
                            + 'Need 3 of each except for adverbs,\n'
                            + 'of which 1 is needed.')
        return False

    # generator
    lbl_poem_title['text'] = f'{a_or_an(adj[0], True)} {noun[0]}'
    lbl_poem['text'] = (f'{a_or_an(adj[0], True)} {noun[0]} {verb[0]} '
                        + f'{prep[0]} the {adj[1]} {noun[1]}\n'
                        + f'{adv[0]}, the {noun[0]} {verb[1]}\n'
                        + f'the {noun[1]} {verb[2]} {prep[1]} '
                        + f'{a_or_an(adj[2], False)} {noun[2]}')
    return True


def save_file():
    output_path = asksaveasfilename(
        defaultextension='txt',
        filetypes=[
            ('Text files', '*.txt'),
            ('All files', '*.*')
        ]
    )

    if not output_path:
        return False

    with open(output_path, 'w') as output_file:
        poem_title = lbl_poem_title['text']
        poem_content = lbl_poem['text']
        rendered_text = f'{poem_title}\n\n{poem_content}'
        output_file.write(rendered_text)

    return True


window = tk.Tk()
window.title('Make your own poem!')
window.resizable(width=True, height=False)

window.columnconfigure(0, weight=1)

# title
lbl_title = tk.Label(
    master=window,
    text='Enter your favorite words, separated by commas.'
)

# input chart
frm_input = tk.Frame(master=window)
frm_input.columnconfigure(0, minsize=100, weight=0)
frm_input.columnconfigure(1, weight=1)

keywords = ['nouns', 'verbs', 'adjectives', 'prepositions', 'adverbs']

for keyword in keywords:
    label_name = f'lbl_{keyword}'
    entry_name = f'ent_{keyword}'
    label_text = f'{keyword.capitalize()}:'

    locals()[label_name] = tk.Label(master=frm_input, text=label_text)
    locals()[entry_name] = tk.Entry(master=frm_input)

    row_index_input = keywords.index(keyword)
    locals()[label_name].grid(row=row_index_input, column=0, sticky='e')
    locals()[entry_name].grid(row=row_index_input, column=1, sticky='ew')

# generate button
btn_gen = tk.Button(master=window, text='Generate', command=poem_gen)

# output frame
frm_output = tk.Frame(
    master=window,
    relief=tk.GROOVE,
    borderwidth=2
)
frm_output.columnconfigure(0, minsize=500, weight=1)

lbl_your_poem = tk.Label(master=frm_output, text='Your poem:')
lbl_poem_title = tk.Label(master=frm_output, text='')
lbl_poem = tk.Label(master=frm_output, text='')
btn_save = tk.Button(master=frm_output, text='Save to file', command=save_file)

output_elements = [lbl_your_poem, lbl_poem_title, lbl_poem, btn_save]
for element in output_elements:
    row_index_output = output_elements.index(element)
    element.grid(row=row_index_output, column=0, pady=5)

# griding main elements
lbl_title.grid(row=0, column=0, pady=5, padx=5)
frm_input.grid(row=1, column=0, sticky='ew', padx=5)
btn_gen.grid(row=2, column=0, pady=5)
frm_output.grid(row=3, column=0, pady=5, padx=5, sticky='nsew')

window.mainloop()
