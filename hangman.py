import random


def output_builder(guessed_list, wrong_list):
    '''Returns game output from given arguments'''
    counter = len(wrong_list)
    
    # head
    if counter == 0:
        head = ''
    else:
        head = '   o'
    # thorax
    if counter < 2:
        thorax = ''
    elif counter == 2:
        thorax = '   |'
    elif counter == 3:
        thorax = '  /|'
    else:
        thorax = '  /|\\'
    # legs
    if counter < 5:
        legs = ''
    elif counter == 5:
        legs = '  /'
    else:
        legs = '  / \\'
        
    # text output
    if counter < 6:
        text1 = ' '.join(guessed_list)
        text2 = ', '.join(wrong_list)
        if not '_' in guessed_list: text2 = 'You win!'
    else:
        text1 = 'You lose.'
        text2 = 'Goodbye.'
        
    # line generator list
    hangman_line_list = [
        f'  _____',
        f'  T   T',
        f'  |{head}\t\t{text1}',
        f'  |{thorax}',
        f'  |{legs}\t\t{text2}',
        f'  |',
        f'TTTTTTTTTT',
        f'',
        ]
    
    return '\n'.join(hangman_line_list) 


# game setup
    
words = [
    'squawk', 'frizzled', 'kiwifruit', 'icebox', 'megahertz', 'crypt',
    'foxglove', 'waxy', 'frazzled', 'xylophone', 'euouae', 'strengths',
    'zombie', 'buffalo', 'pizazz', 'triphthong', 'injury', 'jukebox',
    'phlegm', 'gnostic', 'abruptly', 'daiquiri', 'luxury', 'nymph',
    'affix', 'iatrogenic', 'pshaw', 'dwarves', 'awkward', 'wavy'
    ] # this should be imported I guess

word = random.choice(words)
placeholders = ['_'] * len(word) # generates (empty) character list for game flow
missed_hits = []

print('Try your luck in hangman.')

# game flow

while True:
    print(output_builder(placeholders, missed_hits))
    if len(missed_hits) > 5 or not '_' in placeholders: # aka game over
        break

    # input flow
    while True:
        letter_input = input('Enter a letter: ').lower()
        if len(letter_input) > 1:
            print('Input is too long. Try again.')
            continue
        elif not letter_input.isalpha():
            print('You ought to enter a letter. Try again.')
            continue
        elif letter_input in placeholders or letter_input in missed_hits:
            print(f'You\'ve already tried letter {letter_input.upper()}.')
            continue
        else:
            break

    # hit index iterator
    # asserts proper flow for many instances of the same letter
    try_index = 0
    hit_indexes = []    
    while True:
        try:
            i = word.index(letter_input, try_index)
        except ValueError:
            break
        hit_indexes.append(i)
        try_index = i + 1

    # hit validation    
    if hit_indexes == []:
        missed_hits.append(letter_input)
    else:
        for j in hit_indexes:
            placeholders[j] = word[j]
