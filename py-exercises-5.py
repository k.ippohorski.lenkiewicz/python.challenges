# zadanka z rozdziału 5.

# 5.1
# 5.1-1

num1 = 25000000
num2 = 25_000_000

print(num1)
print(num2)

# 5.1-2

num3 = 175e3
print(num3)

# 5.1-3

# dla mojego komputera rozwiązanie zadania to
print(2e308)

# 5.5
# 5.5-1

num4 = input('Enter a number: ')
num4round2 = round(float(num4), 2)

print(f'{num4} rounded to 2 decimal places is {num4round2}')

# 5.5-2

num5 = input('Enter a number: ')
num5abs = abs(float(num5))

print(f'The absolute value of {num5} is {num5abs}')

# 5.5-3

num6 = input('Enter a number: ')
num7 = input('Enter another number: ')
tester = (float(num6) - float(num7)).is_integer()

print(f'The difference between {num6} and {num7} is an integer? {tester}!')

# 5.6
# 5.6-1

num8 = 3 ** .125
print(f'{num8:.2f}')

# 5.6-2

num9 = 150000
print(f'{num9:,.2f} zł!!!')

# 5.6-3

num10 = 2 / 10
print(f'{num10:.0%}')
