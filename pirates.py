import csv
import matplotlib.pyplot as plt
from pathlib import Path

data_dir = Path.cwd() / 'pirates.csv'

data_set = []

with data_dir.open(mode='r', encoding='utf-8') as data:
    reader = csv.DictReader(data)
    for row in reader:
        data_set.append(row)

years = []
temperatures = []
pirates = []

for entry in data_set:
    years.append(int(entry['Year']))
    temperatures.append(float(entry['Temperature']))
    pirates.append(int(entry['Pirates']))

plt.plot(pirates, temperatures, 'b-X')
plt.title('How number of pirates affect global temperatures?')
plt.ylabel('Global temperature')
plt.xlabel('Number of pirates')
plt.grid()
plt.axis([-2000, 50_000, 14, 16])
plt.yticks(list(range(13,18)))

plt.savefig('pirates.png')
plt.show()
