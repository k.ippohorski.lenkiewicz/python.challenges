def invest(amount, rate, years):
    '''Calculate year-by-year investment growth for a given initial <amount>,
    annual <rate> of return (given in a decimal format), and number of <years>.'''
    for year in range(years):
        amount = amount + amount * rate
        print(f'year {year + 1}: ${amount:,.2f}')
        
print('Hello, welcome to year-by-year investment tracker!')
print('The program will guide you through inputs to return your returns.')
user_amount = float(input('Enter an initial $ amount: '))
user_rate = float(input('Enter annual percentage rate (eg: 0.05): '))
user_years = int(input('Enter investment period in years: '))

invest(user_amount, user_rate, user_years)
