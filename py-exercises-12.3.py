# Zadanka 12.3

# 12.3-1

from pathlib import Path

rt = Path.home() / 'my_folder'
rt.mkdir(exist_ok=True)

# 12.3-2

files = [
    'file1.txt',
    'file2.txt',
    'image1.png',
]

for file in files:
    (rt / file).touch()
    
# 12.3-3

image_dest = rt / 'images' / 'image1.png'

image_dest.parent.mkdir(parents=True, exist_ok=True)

(rt / files[-1]).replace(image_dest)

# 12.3-4

(rt / files[0]).unlink(missing_ok=True)

# 12.3-5

import shutil

shutil.rmtree(rt)
