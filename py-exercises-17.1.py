# Zadanka 17.1

# 17.1-1

import numpy as np

A = np.arange(3,12).reshape(3,3)

# 17.1-2

print(A.min())
print(A.max())
print(A.mean())

# 17.1-3

B = A**2

# 17.1-4

C = np.vstack([A,B])

# 17.1-5

print(C @ A)

# 17.1-6

print(C.reshape(3,3,2))
