# Zadanka 18.7

import tkinter as tk
import random

# 18.7-1

win1 = tk.Tk()

colors = [
    'red',
    'orange',
    'yellow',
    'blue',
    'green',
    'indigo',
    'violet'
    ]

def change_color():
    chosen_color = random.choice(colors)
    btn1['bg'] = chosen_color

btn1 = tk.Button(
    master=win1,
    text='Click me',
    fg='black',
    command=change_color
    )
btn1.pack()

win1.mainloop()

# 18.7-2

win2 = tk.Tk()

d6 = list(range(1, 7))

def dice_roll():
    choice = random.choice(d6)
    dice_lbl['text'] = str(choice)

win2.rowconfigure([0, 1], minsize=50, weight=1)
win2.columnconfigure(0, minsize=150, weight=1)

roll_btn = tk.Button(
    master=win2,
    text='Roll',
    command=dice_roll
    )
roll_btn.grid(row=0, column=0, sticky='nsew')

dice_lbl = tk.Label(
    master=win2,
    text = ''
    )
dice_lbl.grid(row=1, column=0)

win2.mainloop()
