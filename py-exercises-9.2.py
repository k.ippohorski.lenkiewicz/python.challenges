# Zadanka 9.2

# 9.2-1

food = ['rice', 'beans']

# 9.2-2

food.append('broccoli')

# 9.2-3

food.extend(['bread', 'pizza'])

# 9.2-4

print(food[:2])

# 9.2-5

print(food[-1])

# 9.2-6

breakfast = 'eggs, fruit, orange juice'.split(',')

# 9.2-7

print(len(breakfast))

# 9.2-8

lengths = [len(i) for i in breakfast]
print(lengths)

# there are excess ' ' in breakfast list, stripping

breakfast = [i.strip() for i in breakfast]
print(breakfast)

# the lengths changed!!!

lengths = [len(i) for i in breakfast]
print(lengths)
