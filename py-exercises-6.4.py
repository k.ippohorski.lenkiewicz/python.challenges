# Zadanka z rozdziału 6.4

# 6.4-1

for i in range(2, 11):
    print(i)

# 6.4-2

j = 2

while j < 11:
    print(j)
    j = j + 1

# 6.4-3

def doubles(num):
    return num * 2

example = 2

for k in range(3):
    example = doubles(example)
    print(example)


