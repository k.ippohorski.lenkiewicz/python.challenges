# Zadanka 12.6

from pathlib import Path
import csv

# 12.6-1

numbers = [
    [1, 2, 3, 4, 5],
    [6, 7, 8, 9, 10],
    [11, 12, 13, 14, 15],
]

file_path = Path.home() / 'numbers.csv'

with file_path.open(mode='w', encoding='utf-8', newline='') as file:
    writer = csv.writer(file)
    writer.writerows(numbers)

# 12.6-2

numbers = []

with file_path.open(mode='r', encoding='utf-8', newline='') as file:
    reader = csv.reader(file)
    for row in reader:
        numbers.append([int(item) for item in row]) # assuming number-only file

print(numbers)

# 12.6-3

favorite_colors = [
    {'name': 'Joe', 'favorite_color': 'blue'},
    {'name': 'Anne', 'favorite_color': 'green'},
    {'name': 'Bailey', 'favorite_color': 'red'},
]

file_path = Path.home() / 'favorite_colors.csv'

with file_path.open(mode='w', encoding='utf-8', newline='') as file:
    writer = csv.DictWriter(file, favorite_colors[0].keys())
    writer.writeheader()
    writer.writerows(favorite_colors)

# 12.6-4

favorite_colors = []

with file_path.open(mode='r', encoding='utf-8', newline='') as file:
    reader = csv.DictReader(file)
    for row in reader:
        favorite_colors.append(row)

print(favorite_colors)
