from pathlib import Path
import csv


class Inventory:
    inv_dir = Path.cwd() / 'inventory.csv'

    @staticmethod
    def clear_inv():
        """Removes the invesntory file from cwd"""
        if Inventory.inv_dir.exists():
            Inventory.inv_dir.unlink()

    @staticmethod
    def get_inv():
        """Opens inventory (from csv) and gives options to interact"""
        if not Inventory.inv_dir.exists():
            print('**')
            print("**  You don't have any items")
            print('**')
            return False
        with Inventory.inv_dir.open(mode='r', encoding='utf-8', newline='') as inv:
            reader = csv.reader(inv)
            temp_item_dict = {}
            print('||')
            print('|| Items in your inventory:')
            print('||')
            for row in reader:
                temp_item_dict.update({row[0].lower(): row[1]})
                print(f'|| > {row[0]}')
            print('||')
            print('|| Type item\'s name to use it')
            print('|| Type <close> to exit inventory')
            command = input('|| command: ').lower()
            while True:
                if command == 'close':
                    print('||')
                    return False
                if command in temp_item_dict:
                    print('||')
                    print(f'|| {temp_item_dict[command]}')
                    print('||')
                    return command
                command = input('|| Wrong command, try again: ').lower()

    @staticmethod
    def item_found(item):
        """Flow when item is found"""
        for item_name, item_description in item.items():
            with Inventory.inv_dir.open(mode='a', encoding='utf-8', newline='') as inv:
                print('>>')
                print(f'>> You have found {item_name}')
                command = input('>> Type <take> or <leave> to interact with it: ').lower()
                while True:
                    if command == 'take':
                        writer = csv.writer(inv)
                        writer.writerow((item_name, item_description))
                        print('>>')
                        print(f'>> You\'ve added {item_name} to inventory')
                        print('>>')
                        return True
                    elif command == 'leave':
                        print('>>')
                        print(f'>> You\'ve decided to leave {item_name}')
                        print('>>')
                        return False
                    else:
                        command = input('>> Wrong command. Try again: ').lower()
