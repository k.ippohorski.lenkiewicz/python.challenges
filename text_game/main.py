
import game_engine, os
from assets import *
from inventory import Inventory


def clear_console():
    # Check if the operating system is Windows
    if os.name == 'nt':
        os.system('cls')
    else:  # Assuming Unix-like system
        os.system('clear')


# clear console and inventory file before running the game
clear_console()
Inventory.clear_inv()

# move is a tuple: location, bool if coming from sublocation, discovered secrets
move = run_game, False, []

while True:
    move = game_engine.command_exec(move)
    if not move:  # aka game over
        break
    clear_console()
