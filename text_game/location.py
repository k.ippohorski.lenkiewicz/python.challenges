
class Location:
    def __init__(self, data):
        self.visited = False
        self.sublocation = False
        self.intro = False
        self.directions = []
        self.secret_directions = []
        self.secret_code = False
        self.name = data['name']
        self.code = data['code']
        self.text = data['text']
        if 'name_visited' in data:
            self.name_visited = data['name_visited']
        if 'text_visited' in data:
            self.text_visited = data['text_visited']
        if 'items' in data:
            self.items = data['items']
        if 'prompt' in data:
            self.prompt = data['prompt']

    def get_name(self):
        if self.visited and hasattr(self, 'name_visited'):
            return self.name_visited
        else:
            return self.name

    def get_code(self):
        return self.code

    def get_text(self):
        if self.visited and hasattr(self, 'text_visited'):
            return self.text_visited
        else:
            self.visited = True
            return self.text

    def get_items(self):
        if hasattr(self, 'items'):
            return self.items
        else:
            return False

    def get_prompt(self):
        if hasattr(self, 'prompt'):
            return self.prompt
        else:
            return False
        
    def get_directions(self, secrets_discovered):
        if self.directions == []:
            return False
        else:
            dirs = self.directions
            must_visited = []
            if hasattr(self, 'must_visit'):
                for location in self.must_visit:
                    must_visited.append(location.is_visited())
            if self.secret_code != False and all(must_visited):
                if self.secret_code in secrets_discovered:
                    dirs.append(self.secret_direction)
            return {direction.get_code(): direction for direction in dirs}

    def set_directions(self, directions_list):
        for direction in directions_list:
            self.directions.append(direction)

    def is_sublocation(self):
        return self.sublocation

    def item_taken(self):
        del self.items

    def is_intro(self):
        return self.intro

    def set_secret_direction(self, secret_direction, secret_code, must_visit=[]):
        self.secret_direction = secret_direction
        self.secret_code = secret_code
        self.must_visit = must_visit

    def is_visited(self):
        return self.visited
        

class SubLocation(Location):
    def __init__(self, parent_location, data):
        self.prompt = '[press enter to go back]' # before super for keeping secrets
        super().__init__(data)
        self.sublocation = True
        self.parent_location = parent_location
        self.directions = [parent_location]

    def get_parent(self):
        return self.parent_location


class Intro(Location):
    def __init__(self, data):
        super().__init__(data)
        self.intro = True
