

import time, sys
from inventory import Inventory


def command_exec(move):
    
    """
    GAME FLOW
    takes and returns next move and bool if it's coming from sublocation, packed
    """

    # Attention! nested functions inside
    
    def move_list(in_sublocation, secrets_discovered):
        """Prints all possible moves"""
        print('')  # prints new line before output
        for code, direction in move.get_directions(secrets_discovered).items():
            # get what to type
            if code == "" or in_sublocation:
                code = 'Press enter'
            else:
                code = f'Type <{code}>'
            # get where does the code take the player
            if in_sublocation:
                destination = 'go back'
                in_sublocation = False  # resets var if there's more moves
            else:
                destination = direction.get_name()
            # output below
            print(f'  {code} to {destination}')
        if not move.is_intro() and not move.is_sublocation():
            print('  Type <inv> to open inventory')

        print('')  # prints new line before input
        command = input('command: ')
        return command

    def slow_print(lst_of_str):
        """Prints lore letter-by-letter"""
        for string in lst_of_str:
            print('')  # prints new line before output
            for letter in string:
                sys.stdout.write(letter)
                sys.stdout.flush()
                time.sleep(0.1)
            time.sleep(0.8)  # small delay before next line
        print('')  # prints new line after output

    # move execution setup
    move, from_sublocation, secrets_discovered = move  # unpacking
    if not move.get_directions(secrets_discovered):  # aka GAME OVER
        return False
    is_sublocation = move.is_sublocation()
    directions = move.get_directions(secrets_discovered)
    item = move.get_items()

    # lore output
    if from_sublocation:
        slow_print(['*back in the room*'])  # as a list bc slow_print() takes lists
    else:
        slow_print(move.get_text())

    # item flow
    if item != False:
        is_taken = Inventory.item_found(item)
        if is_taken:
            move.item_taken()

    # move list output
    command = move_list(is_sublocation, secrets_discovered).lower()
    
    while True:  # input validity check
        if command in directions:
            return directions[command], False, secrets_discovered
        elif is_sublocation and command == '':
            return move.get_parent(), True, secrets_discovered
        elif command == 'inv':
            item_action = Inventory.get_inv()
            if item_action != False:
                secrets_discovered.append(item_action)
            command = move_list(is_sublocation, secrets_discovered).lower()
        else:
            command = input('Wrong command. Try again: ').lower()
