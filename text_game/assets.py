# text assets container
# assets for a location stored in dict
# obligatory keys: name[to display in help](str), code(str), text(list of str)
# optional keys: text_visited(list of str), items(list of str), prompt(str)

from location import Location, SubLocation, Intro

run_game = Intro({
    'name': '',
    'code': '',
    'text': ["Two Chests",
             "",
             "a game by jachulczyk",
             ],
    })

intro1 = Intro({
    'name': 'start',
    'code': '',
    'text': ["You wake up on a floor in an unfamiliar room,",
             "disoriented and surrounded by darkness.",
             "The faint glow of a couple of candles is your only source of light.",
             "The room is small, cold, and the stone walls give off a musty odor.",
             ],
    })

intro2 = Intro({
    'name': 'continue',
    'code': '',
    'text': ["As you’re getting up you realize that you can't recall how you got here.",
             "Anxiety begins to creep in, and you desperately search for any clues",
             "about your situation or a way out.",
             ],
    })

intro3 = Intro({
    'name': 'continue',
    'code': '',
    'text': ["Your eyes lock onto a wooden door in one corner of the room.",
             "It's slightly ajar, and a feeble line of dim light seeps through the crack.",
             ],
    })

room1 = Location({
    'name': 'go to where you woke up',
    'name_visited': 'go to cell room',
    'code': 'room1',
    'text': ["You take a closer look at the dimly lit nooks of the room where you woke up.",
             "As your eyes adjust to the low light,",
             "you begin to notice some peculiar objects.",
             ],
    'text_visited': ["Feeling helpless you decide to come back to the room where you woke up,",
                     "hoping to find some clues.",
                     ],
    })

room1_book = SubLocation(room1, {
    'name': 'investigate dark corner',
    'code': 'corner',
    'text': ["In a dark corner, you find an old, dusty book, with a title barely legible.",
             "It may contain valuable information or be a clue to your situation.",
             ],
    'text_visited': ["There's nothing more of value here."],
    'items': {'Book': 'You find a quote: "Something pathetic can be the key to your salvation."'}
    })

room1_wall = SubLocation(room1, {
    'name': 'approach the wall',
    'code': 'wall',
    'text': ["There's a series of strange symbols etched into the stone wall.",
             "They appear to be some sort of cryptic message or a code.",
             "You're not sure what they mean, but they might be significant.",
             ],
    })

room1_wall_symbols = SubLocation(room1, {
    'name': 'decypher symbols on the wall',
    'code': 'symbols',
    'text': ["Turning your attention to the strange symbols etched into the stone wall,",
             "you carefully examine each character. However, after meticulous scrutiny,",
             "you realize that these symbols are nothing more than gibberish,",
             "offering no apparent solution to your situation.",
             "Frustration mounts as you struggle to find meaning in them.",
             ],
    })

room1_chest = SubLocation(room1, {
    'name': 'go to a small chest',
    'code': 'chest',
    'text': ["You approach a small chest in the corner. Upon closer inspection,",
             "it becomes evident that the chest is in a state of advanced decay.",
             "In your attempt to open it, you discover that it's brittle and deteriorating.",
             "With a bit of force, the chest crumbles,",
             "revealing nothing of significant value inside.",
             ],
    })

room2 = Location({
    'name': 'investigate the door',
    'name_visited': 'go to chest room',
    'code': 'room2',
    'text': ["You take a deep breath and cautiously approach the door,",
             "your curiosity piqued by what might lie beyond.",
             "You cautiously push the door open and step into the next room.",
             "It's slightly larger than the first and equally dimly lit.",
             "",
             "As you explore the room,",
             "your eyes are immediately drawn to two chests placed on opposite sides.",
             "Both chests have unique etched symbols on them.",
             "One chest features fiery symbols, reminiscent of flames and burning embers,",
             "while the other chest is adorned with icy and snowy symbols,",
             "evoking a sense of cold and frost.",
             "There’s also a strange radiance coming from the wall in front of you.",
             "It’s different than others - carved from a single piece of stone.",
             ],
    'text_visited': ["You decide to venture back into the chest room",
                     "in a renewed attempt to unlock its secrets.",
                     ],
    })

room2_stone_wall = SubLocation(room2, {
    'name': 'examine stone wall',
    'code': 'stone',
    'text': ["Coming closer to the wall you spot two distinct shapes carved out of it.",
             "The first shape resembles a blazing sun,",
             "feels warm to the touch as you run your fingers along its contours,",
             "while the second shape is a serene crescent moon, exuding an aura of coldness,",
             "as though the stone itself carries a hint of the moon's tranquil energy.",
             ],
    'text_visited': ["While the meanings of these carved shapes remain a mystery,",
                     "they seem to represent opposing elements – fire and ice, day and night,",
                     "perhaps even life and death.",
                     "You ponder the significance of these symbols and wonder",
                     "if they hold the key to your situation or if they are simply a part",
                     "of the enigmatic puzzle that surrounds you.",
                     "There’s not much more you can do here.",
                     ],
    })

room2_ice_chest = SubLocation(room2, {
    'name': 'go to ice chest',
    'code': 'ice',
    'text': ["You decide to approach the chest adorned with icy and snowy symbols.",
             "As you carefully open the lid, a blast of icy cold air pours out of the chest,",
             "sending a shiver down your spine.",
             "You can see your breath as frost quickly spreads through the room,",
             "and the chill pierces through your body. The chest appears to be filled with snow.",
             "It becomes clear that you cannot keep the chest open for long,",
             "or you risk freezing to death.",
             "The lid falls close from your frostbitten hands.",
             ],
    'text_visited': ["Turning your attention to the icy chest,",
                     "you open it, and the same frigid air pours out, just as it did before.",
                     "You recall that leaving it open for too long would freeze your body.",
                     ],
    })

room2_fire_chest = SubLocation(room2, {
    'name': 'go to fire chest',
    'code': 'fire',
    'text': ["You decide to approach the chest adorned with fiery symbols.",
             "As you carefully open the lid, an intense and scorching heat pours out,",
             "causing you to wince in discomfort.",
             "The chest is filled with a blazing inferno, and flames leap out,",
             "filling the room with searing heat and fiery light.",
             "The flames start crawling up your arms and legs,",
             "licking at your skin with their fiery tongues.",
             "Leaving the chest open for too long would undoubtedly result",
             "in your body being engulfed by the searing flames.",
             "You can’t get a hold of the burning lid and it falls close.",
             ],
    'text_visited': ["You approach the fiery chest first.",
                     "As you open the lid, the intense heat and flames burst forth, much like before.",
                     "You remember that attempting to keep the chest open",
                     "would result in your body being consumed by the scorching flames.",
                     ],
    })

room2_secret = Location({
    'name': 'ponder on the mysterious quote',
    'code': 'idea',
    'text': ["As the mysterious quote from the old book lingers in your thoughts —",
             "\"Something pathetic can be the key to your salvation\"",
             "you suddenly realize that perhaps the solution lies in an action rather than an object.",
             "Inspired by the quote, you feel an urge to rearrange the elements in the room.",
             "The chests, adorned with fire and ice, catch your attention.",
             "Could moving them together in the middle of the room reveal the path to salvation?"
             ],
    })

room2_chest_action = Location({
    'name': 'move the chests together',
    'code': 'move',
    'text': ["As you carefully shift the fiery and icy chests, you feel a strange resonance in the air,",
             "as if the room itself responds to your actions.",
             "You feel that this might be the moment to try something risky and open both chests.",
             "As the fiery and icy chests reveal their contents, a surprising phenomenon occurs.",
             "The intense heat from the fiery chest and the freezing cold from the icy chest",
             "seem to counteract each other, creating a harmonious balance.",
             "To your amazement, the chests hold the missing pieces -",
             "a blazing sun element from the fiery chest",
             "and a serene crescent moon element from the icy chest.",
             "It becomes clear that these elements are meant to fit into the shapes on the wall.",
             "The path forward starts to unfold as you hold the sun and moon elements in your hands.",
             ],
    })

room2_wall_action = Location({
    'name': 'place the pieces in the wall',
    'code': 'place',
    'text': ["As you place the blazing sun and serene crescent moon elements",
             "into the corresponding shapes on the wall, you feel a surge of energy in the room.",
             "The carved symbols come to life, glowing with newfound brilliance.",
             "The room trembles as if acknowledging the completion of the puzzle.",
             "Suddenly, a hidden door creaks open, revealing a path to freedom.",
             "The cryptic quote, the fiery and icy chests, and the elements have all led to this moment.",
             "You step through the doorway, leaving the enigmatic room behind, victorious in unraveling its secrets.",
             "Congratulations! You have successfully solved the puzzle and escaped the mysterious room.",
             ],
    })

ending = Location({
    'name': 'end',
    'code': '',
    'text': [''],
    })

# setting possible moves for instances below

run_game.set_directions([intro1])
intro1.set_directions([intro2])
intro2.set_directions([intro3])
intro3.set_directions([room1, room2])
room1.set_directions([room2, room1_book, room1_wall, room1_chest])
room1_wall.set_directions([room1_wall_symbols])
room2.set_directions([room1, room2_stone_wall, room2_ice_chest, room2_fire_chest])
room2.set_secret_direction(room2_secret, 'book', [room2_stone_wall, room2_ice_chest, room2_fire_chest])
room2_secret.set_directions([room2_chest_action])
room2_chest_action.set_directions([room2_wall_action])
room2_wall_action.set_directions([ending])

