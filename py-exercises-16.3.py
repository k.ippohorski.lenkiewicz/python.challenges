# Zadanka 16.3

# 16.3-1

import mechanicalsoup

browser = mechanicalsoup.Browser()
url = 'http://olympus.realpython.org/login'
login_page = browser.get(url)
login_html = login_page.soup

form = login_html.select('form')[0]
form.select('input')[0]['value'] = 'zeus'
form.select('input')[1]['value'] = 'ThunderDude'

profiles_page = browser.submit(form, login_page.url)

# 16.3-2

print(profiles_page.soup.title.get_text())

# 16.3-3

new_login_page = browser.get(url)
new_login_html = new_login_page.soup

# 16.3-4

new_form = new_login_html.select('form')[0]
new_form.select('input')[0]['value'] = 'bla'
new_form.select('input')[1]['value'] = 'blabla'

resulting_page = browser.submit(new_form, new_login_page.url)
scraped_text = resulting_page.soup.get_text()

test = 'Wrong username or password!'

if scraped_text.find(test) != -1:
    print('Login failed successfully.')
