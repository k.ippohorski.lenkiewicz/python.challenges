# Zadanka 18.6

# 18.6-1

import tkinter as tk

# screenshot 1

win1 = tk.Tk()

frm1_1 = tk.Frame(
    master=win1,
    width=50,
    height=50,
    bg='red'
)
frm1_1.pack()

frm1_2 = tk.Frame(
    master=win1,
    width=25,
    height=25,
    bg='yellow'
)
frm1_2.pack()

frm1_3 = tk.Frame(
    master=win1,
    width=10,
    height=10,
    bg='blue'
)
frm1_3.pack()

win1.mainloop()

# screenshot 2

win2 = tk.Tk()

frm2_1 = tk.Frame(
    master=win2,
    width=50,
    height=50,
    bg='red'
)
frm2_1.pack(fill=tk.X, expand=True)

frm2_2 = tk.Frame(
    master=win2,
    height=25,
    bg='yellow'
)
frm2_2.pack(fill=tk.X, expand=True)

frm2_3 = tk.Frame(
    master=win2,
    height=10,
    bg='blue'
)
frm2_3.pack(fill=tk.X, expand=True)

win2.mainloop()

# screenshot 3

win3 = tk.Tk()

frm3_1 = tk.Frame(
    master=win3,
    width=50,
    height=50,
    bg='red'
)
frm3_1.pack(fill=tk.BOTH, side=tk.LEFT, expand=True)

frm3_2 = tk.Frame(
    master=win3,
    width=25,
    bg='yellow'
)
frm3_2.pack(fill=tk.BOTH, side=tk.LEFT, expand=True)

frm3_3 = tk.Frame(
    master=win3,
    width=10,
    bg='blue'
)
frm3_3.pack(fill=tk.BOTH, side=tk.LEFT, expand=True)

win3.mainloop()

# screenshot 4

win4 = tk.Tk()

frm4 = tk.Frame(
    master=win4,
    width=300,
    height=200
)
frm4.pack(side=tk.LEFT)

lbl4_1 = tk.Label(
    master=frm4,
    text='I\'m at 0,0',
    bg='red'
)
lbl4_1.place(x=0, y=0)

lbl4_2 = tk.Label(
    master=frm4,
    text='I\'m at 75,75',
    bg='yellow',
    fg='black'
)
lbl4_2.place(x=75, y=75)

win4.mainloop()

# screenshot 5

win5 = tk.Tk()

for i in range(3):
    for j in range(3):
        frame = tk.Frame(
            master=win5,
            relief=tk.RAISED,
            borderwidth=1
        )
        frame.grid(row=i, column=j)
        label = tk.Label(master=frame, text=f'row{i} / column{j}')
        label.pack()

win5.mainloop()

# screenshot 6

win6 = tk.Tk()

for i in range(3):
    for j in range(3):
        frame = tk.Frame(
            master=win6,
            relief=tk.RAISED,
            borderwidth=1
        )
        frame.grid(row=i, column=j, padx=5, pady=5)
        label = tk.Label(master=frame, text=f'row{i} / column{j}')
        label.pack()

win6.mainloop()

# screenshot 7

win7 = tk.Tk()

for i in range(3):
    for j in range(3):
        frame = tk.Frame(
            master=win7,
            relief=tk.RAISED,
            borderwidth=1
        )
        frame.grid(row=i, column=j, padx=5, pady=5)
        label = tk.Label(master=frame, text=f'row{i} / column{j}')
        label.pack(padx=5, pady=5)

win7.mainloop()

# screenshot 8

win8 = tk.Tk()
win8.columnconfigure(0, minsize=250)
win8.rowconfigure([0, 1], minsize=100)

lbl8_1 = tk.Label(text='A')
lbl8_1.grid(row=0, column=0)

lbl8_2 = tk.Label(text='B')
lbl8_2.grid(row=1, column=0)

win8.mainloop()

# screenshot 9

win9 = tk.Tk()
win9.columnconfigure(0, minsize=250)
win9.rowconfigure([0, 1], minsize=100)

lbl9_1 = tk.Label(text='A')
lbl9_1.grid(row=0, column=0, sticky='n')

lbl9_2 = tk.Label(text='B')
lbl9_2.grid(row=1, column=0, sticky='n')

win9.mainloop()

# screenshot 10

win10 = tk.Tk()
win10.columnconfigure(0, minsize=250)
win10.rowconfigure([0, 1], minsize=100)

lbl10_1 = tk.Label(text='A')
lbl10_1.grid(row=0, column=0, sticky='ne')

lbl10_2 = tk.Label(text='B')
lbl10_2.grid(row=1, column=0, sticky='sw')

win10.mainloop()

# screenshot 11

win11 = tk.Tk()

column_indexes = [0, 1, 2, 3]

win11.rowconfigure(0, minsize=100)
win11.columnconfigure(column_indexes, minsize=100)

stickies = ['', 'ew', 'ns', 'nsew']

for i in column_indexes:
    label = tk.Label(
        text=str(i+1),
        bg='black',
        fg='white'
    )
    label.grid(
        row=0,
        column=i,
        sticky=stickies[i]
    )

win11.mainloop()

# 18.6-2

win12 = tk.Tk()

win12.minsize(500, 280)

win12.rowconfigure(0, minsize=240)
win12.rowconfigure(1, minsize=40)
win12.columnconfigure(0, minsize=500)

# top container
frm12_1 = tk.Frame(
    master=win12,
    relief=tk.SUNKEN,
    borderwidth=2
)

row_indexes = list(range(8))
row_labels = [
    'First Name:',
    'Last Name:',
    'Address Line 1:',
    'Address Line 2:',
    'City:',
    'State/Province:',
    'Postal Code:',
    'Country:'
]

frm12_1.rowconfigure(
    row_indexes,
    minsize=30
)
frm12_1.columnconfigure(0, minsize=100)
frm12_1.columnconfigure(1, minsize=400)

for i in row_indexes:
    chart_label = tk.Label(
        master=frm12_1,
        text=row_labels[i]
    )
    chart_label.grid(
        row=i,
        column=0,
        sticky='e'
    )
    chart_entry = tk.Entry(master=frm12_1)
    chart_entry.grid(
        row=i,
        column=1,
        sticky='nswe'
    )

frm12_1.grid(row=0, column=0, sticky='nsew')

# bottom container
frm12_2 = tk.Frame(master=win12)

button_labels = ['Submit', 'Clear']
for i in button_labels:
    button = tk.Button(
        master=frm12_2,
        text=i,
        padx=5,
        pady=5
    )
    button.pack(side=tk.RIGHT)

frm12_2.grid(row=1, column=0, sticky='nsew')

win12.mainloop()
