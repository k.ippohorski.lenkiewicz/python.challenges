# Zadanka 10.3

# 10.3-1

class Dog:
    species = 'Canis familiaris'
    
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return f'{self.name} is {self.age} years old'

    def speak(self, sound):
        return f'{self.name} says {sound}'

class GoldenRetriever(Dog):
    def speak(self, sound='Bark'):
        return super().speak(sound)

# 10.3-2

class Rectangle:
    def __init__(self, length, width):
        self.length = length
        self.width = width

    def area(self):
        return self.length * self.width

class Square(Rectangle):
    def __init__(self, side_length):
        self.length = side_length
        self.width = side_length

square = Square(4)
print(square.area())

square.width = 5
print(square.area())
