nouns = [
    'fossil',
    'horse',
    'aardvark',
    'Judge',
    'chef',
    'rango',
    'extrovert',
    'gorilla'
]

verbs = [
    'kicks',
    'jingles',
    'bounces',
    'slurps',
    'meows',
    'explodes',
    'curdles'
]

adjectives = [
    'furry',
    'balding',
    'incredulous',
    'fragrant',
    'exuberant',
    'glistening'
]

prepositions = [
    'against',
    'after',
    'into',
    'beneath',
    'upon',
    'for',
    'in',
    'like',
    'over',
    'within'
]

adverbs = [
    'curiously',
    'extravagantly',
    'tantalizingly',
    'furiously',
    'sensuously'
]

import random

def a_or_an(word, is_capital):
    ''' Returns determiner with the word. Specify if it should be capital.'''
    an_letters = ('a', 'e', 'i', 'o', 'u')
    word_lowered = word.lower()
    determiner = 'a'
    if is_capital:
        determiner = 'A'
    if word_lowered[0] in an_letters:
        return f'{determiner}n {word}'
    else:
        return f'{determiner} {word}'
        

poem_setup = [
    [nouns, 3, 'noun'],
    [verbs, 3, 'verb'],
    [adjectives, 3, 'adj'],
    [prepositions, 2, 'prep'],
    [adverbs, 1, 'adverb']
]

for words in poem_setup:
    for i in range(words[1]):
        locals()[words[2] + str(i+1)] = random.choice(words[0]) # verb1 etc..

print(f'{a_or_an(adj1, True)} {noun1}')
print()
print(f'{a_or_an(adj1, True)} {noun1} {verb1} {prep1} the {adj2} {noun2}')
print(f'{adverb1}, the {noun1} {verb2}')
print(f'the {noun2} {verb3} {prep2} {a_or_an(adj3, False)} {noun3}')
print()
