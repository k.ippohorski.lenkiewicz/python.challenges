# Zadanka 9.6

# 9.6-1

captains = {}

# 9.6-2

captains['Enterprise'] = 'Picard'
captains['Voyager'] = 'Janeway'
captains['Defiant'] = 'Sisko'

# 9.6-3

if not 'Enterprise' in captains:
    captains['Enterprise'] = 'unknown'

if not 'Discovery' in captains:
    captains['Discovery'] = 'unknown'

# 9.6-4

for ship, cap in captains.items():
    print(f'The {ship} in captained by {cap}')

# 9.6-5

del captains['Discovery']

# 9.6-6

captains2 = dict((('Enterprise', 'Picard'),('Voyager', 'Janeway'),('Defiant', 'Sisko')))
