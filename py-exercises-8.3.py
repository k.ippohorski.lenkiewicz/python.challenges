# Zadanka z rozdziału 8.3

# 8.3-1

word = input('Enter a word: ')

if len(word) == 5:
    print('Your input is 5 characters long')
elif len(word) < 5:
    print('Your input is less than 5 characters long')
else:
    print('Your input is greater than 5 characters long')

# 8.3-2

print('I\'m thinking of a number between 1 and 10. Guess which one.')
guess = int(input('Your guess: '))

if guess == 3:
    print('You win!')
else:
    print('You lose.')
