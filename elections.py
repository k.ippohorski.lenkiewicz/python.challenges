import random

def election_reg(chances):
    if random.random() < chances:
        return 1
    else:
        return 0

def election_city():
    election_outcome = 0
    election_outcome += election_reg(reg_1)
    election_outcome += election_reg(reg_2)
    election_outcome += election_reg(reg_3)
    # zrobiłbym to pętlą, ale 'niby' ich jeszcze nie znam :D
    if election_outcome >= 2:
        return 1
    else:
        return 0

reg_1 = 0.87
reg_2 = 0.65
reg_3 = 0.17

trials = 10_000
a_win_count = 0

for i in range(trials):
    a_win_count += election_city()

a_winning_percent = a_win_count / trials

print(f'Candidate A has a {a_winning_percent:.2%} chance of winning the elections.')
