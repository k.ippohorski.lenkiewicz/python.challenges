# zadanka z książki
# po kolei, zaczynamy od rozdziału 4.

# 4.1
# 4.1-1

print('Tekst w "cudzysłowie"')

# 4.1-2

print("Tekst z apostrofą'")

# 4.1-3

print("""To będzie
        tekst
    w kilku linijkach""")

# 4.1-4

print('Tekst w kliku linijkach \
ale nie w \
outpucie')

# 4.2
# 4.2-1

print(len('ile to literek'))

# 4.2-2

str1 = 'Na '
str2 = 'początek.'
result_str1 = str1 + str2
print(result_str1)

# 4.2-3

str3 = 'Bez'
str4 = 'przerwy.'
result_str2 = str3 + ' ' + str4
print(result_str2)

# 4.2-4

str5 = 'bazinga'
print(str5[2:-1])

# 4.3
# 4.3-1

str6 = 'Animals'
str7 = 'Badger'
str8 = 'Honey Bee'
str9 = 'Honey Badger'

print(str6.lower())
print(str7.lower())
print(str8.lower())
print(str9.lower())

# 4.3-2

print(str6.upper())
print(str7.upper())
print(str8.upper())
print(str9.upper())

# 4.3-3

str10 = '   Filet Mignon'
str11 = 'Brisket   '
str12 = '   Cheeseburger   '

str10 = str10.lstrip()
str11 = str11.rstrip()
str12 = str12.strip()

print(str10)
print(str11)
print(str12)

# 4.3-4

str13 = 'Becomes'
str14 = 'becomes'
str15 = 'BEAR'
str16 = '   bEautiful'

print(str13.startswith('be'))
print(str14.startswith('be'))
print(str15.startswith('be'))
print(str16.startswith('be'))

# 4.3-5

str13 = str13.lower()
str15 = str15.lower()
str16 = str16.strip().lower()

print(str13.startswith('be'))
print(str14.startswith('be'))
print(str15.startswith('be'))
print(str16.startswith('be'))

# 4.4
# 4.4-1

input1 = input('Pisz tu: ')
print('Napisałeś: ' + input1)

# 4.4-2

input2 = input('Pisz tu dużymi literami: ')
print('Napisałeś: ' + input2.lower() + ' :)')

# 4.4-3

input3 = input('Policzę długość tego tekstu: ')
print('Napisałeś tyle znaków: ' + str(len(input3)))

# 4.5 w osobnym pliku > first_letter.py

# 4.6
# 4.6-1

num1 = '4'
num1 = int(num1)
print(num1 * 3)

# 4.6-2

num2 = '4.0'
num2 = float(num2)
print(num2 * 3)

# 4.6-3

str17 = 'siedemnastka'
num3 = 17
print(str17 + str(num3))

# 4.6-4

num4 = input('Pierwsza liczba do pomnożenia: ')
num5 = input('Druga liczba do pomnożenia: ')

print('The product of ' + num4 + ' and ' + num5 + ' is ' + str(float(num4) * float(num5)) + '.')

# 4.7
# 4.7-1

weight = 0.2
animal = 'newt'

print(str(weight) + ' kg is the weight of the ' + animal + '.')

# 4.7-2

print('{} kg is the weight of the {}.'.format(weight, animal))

# 4.7-3

print(f'{weight} kg is the weight of the {animal}.')

# 4.8
# 4.8-1

print('AAA'.find('a'))

# 4.8-2

str18 = 'Somebody said something to Samantha'
str18 = str18.replace('s', 'x')
str18 = str18.replace('S', 'X')
print(str18)

# 4.8-3

str19 = input('Proszę o trochę tekstu: ')
print(str19.find('a'))

# 4.9 w osobnym pliku > translate.py
