
def bsort(data):

    if not isinstance(data, list):
        return False

    arr = data
    cycles_num = len(arr)-1
    for i in range(cycles_num):
        for j in range(cycles_num-i):
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]
            print(arr)
    return arr
