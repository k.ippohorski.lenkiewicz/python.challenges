# Zadanko 18.9

import tkinter as tk
from tkinter.filedialog import askopenfilename, asksaveasfilename
from pathlib import Path

def open_file():
    input_path = askopenfilename(
        filetypes=[
            ('Text files', '*.txt'),
            ('All files', '*.*')
        ]
    )

    if not input_path:
        return False

    txt.delete('1.0', tk.END)

    with open(input_path, 'r') as input_file:
        content = input_file.read()
        txt.insert(tk.END, content)

    file_name = Path(input_path).name
    window.title(f'Text editor – {file_name}')
    return True

def save_file():
    output_path = asksaveasfilename(
        defaultextension='txt',
        filetypes=[
            ('Text files', '*.txt'),
            ('All files', '*.*')
        ]
    )

    if not output_path:
        return False

    with open(output_path, 'w') as output_file:
        content = txt.get('1.0', tk.END)
        output_file.write(content)

    file_name = Path(output_path).name
    window.title(f'Text editor – {file_name}')
    return True


window = tk.Tk()
window.title('Text editor')
window.minsize(width=800, height=800)

window.rowconfigure(0, minsize=800, weight=1)
window.columnconfigure(0, weight=0)
window.columnconfigure(1, weight=1)

# buttons
frm_buttons = tk.Frame(
    master=window,
    width=100
)
btn_open = tk.Button(
    master=frm_buttons,
    text='Open',
    command=open_file
)
btn_save = tk.Button(
    master=frm_buttons,
    text='Save as...',
    command=save_file
)

btn_open.grid(row=0, column=0, sticky='ew')
btn_save.grid(row=1, column=0, sticky='ew')

# text frame
txt = tk.Text(master=window)

frm_buttons.grid(row=0, column=0, padx=5, pady=5, sticky='new')
txt.grid(row=0, column=1, sticky='nsew')

window.mainloop()
