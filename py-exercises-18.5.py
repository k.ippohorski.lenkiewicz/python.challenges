# Zadanka 18.5

# 18.5-1

import tkinter as tk

# screenshot 1

window_1 = tk.Tk()

lbl_1 = tk.Label(
    text='Hello, Tkinter',
    bg='black',
    fg='white',
    height=10
)

lbl_1.pack()

window_1.mainloop()

# screenshot 2

window_2 = tk.Tk()

btn_2 = tk.Button(
    text='Click me!',
    bg='blue',
    fg='yellow',
    width=20,
    height=5,
)

btn_2.pack()

window_2.mainloop()

# screenshot 3

window_3 = tk.Tk()

lbl_3 = tk.Label(text='Name')
ent_3 = tk.Entry(width=20)

lbl_3.pack()
ent_3.pack()

window_3.mainloop()

# screenshot 4

window_4 = tk.Tk()

lbl_4 = tk.Label(text='Name')
ent_4 = tk.Entry(width=20)

lbl_4.pack()
ent_4.pack()

ent_4.insert(0, 'Real Python')

window_4.mainloop()

# screenshot 5

window_5 = tk.Tk()

lbl_5 = tk.Label(text='Name')
ent_5 = tk.Entry(width=20)

lbl_5.pack()
ent_5.pack()

ent_5.insert(0, 'Real Python')
ent_5.delete(0)

window_5.mainloop()

# screenshot 6

window_6 = tk.Tk()

lbl_6 = tk.Label(text='Name')
ent_6 = tk.Entry(width=20)

lbl_6.pack()
ent_6.pack()

ent_6.insert(0, 'Real Python')
ent_6.delete(0, 5)

window_6.mainloop()

# screenshot 7

window_7 = tk.Tk()

lbl_7 = tk.Label(text='Name')
ent_7 = tk.Entry(width=20)

lbl_7.pack()
ent_7.pack()

ent_7.insert(0, 'Real Python')
ent_7.delete(0, tk.END)

window_7.mainloop()

# screenshot 8

window_8 = tk.Tk()

lbl_8 = tk.Label(text='Name')
ent_8 = tk.Entry(width=20)

lbl_8.pack()
ent_8.pack()

ent_8.insert(0, 'Real Python')
ent_8.delete(0, tk.END)
ent_8.insert(0, 'Python')

window_8.mainloop()

# screenshot 9

window_9 = tk.Tk()

lbl_9 = tk.Label(text='Name')
ent_9 = tk.Entry(width=20)

lbl_9.pack()
ent_9.pack()

ent_9.insert(0, 'Real Python')
ent_9.delete(0, tk.END)
ent_9.insert(0, 'Python')
ent_9.insert(0, 'Real ')

window_9.mainloop()

# screenshot 10

window_10 = tk.Tk()

txt_10 = tk.Text(width=50, height=20)
txt_10.pack()

txt_10.insert('1.0', 'Hello\nWorld')

window_10.mainloop()

# screenshot 11

window_11 = tk.Tk()

txt_11 = tk.Text(width=50, height=20)
txt_11.pack()

txt_11.insert('1.0', 'Hello\nWorld')
txt_11.delete('1.0')

window_11.mainloop()

#screenshot 12

window_12 = tk.Tk()

txt_12 = tk.Text(width=50, height=20)
txt_12.pack()

txt_12.insert('1.0', 'Hello\nWorld')
txt_12.delete('1.0', '1.5')

window_12.mainloop()

# screenshot 13

window_13 = tk.Tk()

txt_13 = tk.Text(width=50, height=20)
txt_13.pack()

txt_13.insert('1.0', 'Hello\nWorld')
txt_13.delete('1.0', '2.0')

window_13.mainloop()

# screenshot 14

window_14 = tk.Tk()

txt_14 = tk.Text(width=50, height=20)
txt_14.pack()

txt_14.insert('1.0', 'Hello\nWorld')
txt_14.delete('1.0', tk.END)
txt_14.insert('1.0', 'Hello')

window_14.mainloop()

# screenshot 15

window_15 = tk.Tk()

txt_15 = tk.Text(width=50, height=20)
txt_15.pack()

txt_15.insert('1.0', 'Hello\nWorld')
txt_15.delete('1.0', tk.END)
txt_15.insert('1.0', 'Hello')
txt_15.insert(tk.END, 'World')

window_15.mainloop()

# screenshot 16

window_16 = tk.Tk()

txt_16 = tk.Text(width=50, height=20)
txt_16.pack()

txt_16.insert('1.0', 'Hello\nWorld')
txt_16.delete('1.0', tk.END)
txt_16.insert('1.0', 'Hello')
txt_16.insert(tk.END, '\nWorld')

window_16.mainloop()

# screenshot 17

window_17 = tk.Tk()

frm_17 = tk.Frame()
frm_17.pack()

window_17.mainloop()

# screenshot 18

window_18 = tk.Tk()

frm_18_a = tk.Frame()
lbl_18_a = tk.Label(master=frm_18_a, text='I\'m in Frame A')
lbl_18_a.pack()


frm_18_b = tk.Frame()
lbl_18_b = tk.Label(master=frm_18_a, text='I\'m in Frame B')
lbl_18_b.pack()

frm_18_a.pack()
frm_18_b.pack()

window_18.mainloop()

# screenshot 19

window_19 = tk.Tk()

frm_19_a = tk.Frame()
lbl_19_a = tk.Label(master=frm_19_a, text='I\'m in Frame A')

frm_19_b = tk.Frame()
lbl_19_b = tk.Label(master=frm_19_a, text='I\'m in Frame B')

lbl_19_b.pack()
frm_19_b.pack()

lbl_19_a.pack()
frm_19_a.pack()

window_19.mainloop()

# screenshot 20

borders = {
    'flat': tk.FLAT,
    'sunken': tk.SUNKEN,
    'raised': tk.RAISED,
    'groove': tk.GROOVE,
    'ridge': tk.RIDGE
}

window_20 = tk.Tk()

for relief_name, relief in borders.items():
    frame = tk.Frame(master=window_20, relief=relief, borderwidth=5)
    frame.pack(side=tk.LEFT)
    label = tk.Label(master=frame, text=relief_name)
    label.pack()

window_20.mainloop()


# 18.5-2

window_21 = tk.Tk()

btn_21 = tk.Button(
    text='Click here',
    width=50,
    height=25,
    bg='white',
    fg='blue'
)

btn_21.pack()

window_21.mainloop()

# 18.5-3

window_22 = tk.Tk()

ent_22 = tk.Entry(
    width=40,
    bg='white',
    fg='black'
)

ent_22.pack()
ent_22.insert(0, 'What is your name?')

window_22.mainloop()

