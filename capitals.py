capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
}

import random

state, capital = random.choice(list(capitals_dict.items()))

while True:
    answer = input(f'Please enter capital of {state}: ').lower()
    if answer == capital.lower():
        print('Correct!')
        break
    elif answer == 'exit':
        print(f'The capital of {state} is {capital}. Goodbye.')
        break
