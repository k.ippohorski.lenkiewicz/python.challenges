

import easygui as gui
from pypdf import PdfReader, PdfWriter
# using PyPDF 3.17.4

# 1. file selection
input_path = gui.fileopenbox(
    title='Select a PDF to rotate...',
    default='*.pdf'
)

# 2. exit if no path
if input_path is None:
    exit()

# 3. choose rotation degree
choices_tuple = ('90', '180', '270')

degrees = None
while degrees is None:
    degrees = gui.buttonbox(
        msg='Rotate the PDF clockwise by how many degrees?',
        title='Choose rotation...',
        choices=choices_tuple,
    )

degrees = int(degrees)

# 4. where to save
save_title = 'Save the rotated PDF as...'
file_type = '*.pdf'
output_path = gui.filesavebox(title=save_title, default=file_type)

# 5. prevent overwriting
while input_path == output_path:
    gui.msgbox(msg='Cannot overwrite the original file!')
    output_path = gui.filesavebox(title=save_title, default=file_type)

# 6. exit if no output
if output_path is None:
    exit()

# 7. perform rotation
input_file = PdfReader(input_path)
output_pdf = PdfWriter()

for page in input_file.pages:
    page = page.rotate(degrees)
    output_pdf.add_page(page)

with open(output_path, 'wb') as output_file:
    output_pdf.write(output_file)

# 8. success
gui.msgbox(msg='PDF successfully rotated!')
