

import os
import random
import time
from checkers import Board


def clear_console_and_print_board(board, override=False, print_board=True):
    if board.player_move or override:
        # Check if the operating system is Windows
        if os.name == 'nt':
            os.system('cls')
        else:  # Assuming Unix-like system
            os.system('clear')
        if print_board:
            board.print_board()

game = Board()
clear_console_and_print_board(game, True, False)
game.start_game()

while True:
    clear_console_and_print_board(game, True)
    move_options = game.get_move_options()
    if not move_options:
        break
    game.set_cell_decorators(move_options, True)
    clear_console_and_print_board(game)
    cell_choice = game.get_choice()
    moves_for_selected = game.get_moves_for_selected(move_options, cell_choice)
    game.set_cell_decorators(moves_for_selected, False)
    clear_console_and_print_board(game)
    target_cell = game.get_choice()
    chosen_move = game.select_move(moves_for_selected, target_cell)
    for step in chosen_move:
        time.sleep(0.5)
        game.move_piece(step)
        clear_console_and_print_board(game, True)
        time.sleep(0.5)
    game.toggle_player()

if game.player_move:
    print('You lost. Game over.\n')
else:
    print('You win! Game over.\n')
