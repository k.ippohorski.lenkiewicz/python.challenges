
import string
import random
import copy


class Board:

    def __init__(self):
        self.board_size = 8
        self.row_labels = [i+1 for i in range(self.board_size)]
        self.row_labels.reverse()
        self.col_labels = list(string.ascii_uppercase)[:self.board_size]
        self.matrix = []
        for i in self.row_labels:
            row = []
            # setting the board with empty cells
            for _ in self.col_labels:
                row.append(False)
            # marking out white (unplayable) cells
            if i % 2 == 1:
                empty_cell_at_index = 1
            else:
                empty_cell_at_index = 0
            for k in range(empty_cell_at_index, self.board_size, 2):
                row[k] = None
            self.matrix.append(row)
        self.BOARD_BOUNDS = (0, (self.board_size - 1))
        self.targeted_cells = []
        self.jumped_cells = []
        self.aux_cells = []
        self.selected_cell = None
        self.player_color = None
        self.opponent_color = None
        self.player_move = None

    def start_game(self):

        def choose_color():
            colors = 'white', 'black'
            print('Choose your color.')
            while True:
                choice = input('type [white / black / random]: ').lower()
                if choice in colors:
                    return choice
                elif choice == 'random':
                    return random.choice(colors)
                else:
                    print('Wrong answer. Try again.')

        def init_pieces(rows, piece_color):
            for row_index in rows:
                for col_index in range(self.board_size):
                    if self.matrix[row_index][col_index] is not None:
                        self.matrix[row_index][col_index] = piece_color

        print('\nEnglish Checkers')
        print('\na game by jachulczyk')
        print('\nRules overview:' +
              '\nRegular pieces can only jump forwards' +
              '\nKings gain ability to move and jump backwards,' +
              '\nbut only one cell at a time.' +
              '\nJumping is obligatory, multi-jumps are legal,' +
              '\nand if possible, longest multi-jump is obligatory.' +
              '\n\nFor more visit:' +
              '\nhttps://en.wikipedia.org/wiki/English_draughts')

        self.print_board()
        initial_input = input('Press enter to start\n')

        self.player_color = choose_color()
        if self.player_color == 'white':
            self.opponent_color = 'black'
            self.player_move = True
        else:
            self.opponent_color = 'white'
            self.player_move = False

        player_rows = (5, 6, 7)
        opponent_rows = (0, 1, 2)

        init_pieces(player_rows, self.player_color[0])
        init_pieces(opponent_rows, self.opponent_color[0])

    def print_board(self):

        def print_footer():
            print(' ' * 4, end='')
            for i in range(len(self.col_labels)):
                print(self.col_labels[i] + ' ', end='')
            print('')

        def print_cell(row, col):
            cell_to_print = self.matrix[row][col]
            if cell_to_print is False:
                value = '-'
            elif cell_to_print == 'w':
                value = '⛀'
            elif cell_to_print == 'W':
                value = '⛁'
            elif cell_to_print == 'b':
                value = '⛂'
            elif cell_to_print == 'B':
                value = '⛃'
            else:
                return ' '
            if (row, col) == self.selected_cell:
                return f'[{value}]'
            elif (row, col) in self.targeted_cells:
                return f'>{value}<'
            elif (row, col) in self.jumped_cells:
                return f'x{value}x'
            elif (row, col) in self.aux_cells:
                return f'#{value}#'
            else:
                return f' {value} '

        def print_row(row):
            label = row_label(row)
            print(label, end='')
            for col_index in range(self.board_size):
                cell_output = print_cell(row, col_index)
                print(cell_output, end='')
            print('')

        def row_label(row):
            label = self.row_labels[row]
            if row % 2 == 0:
                suffix = '  '
            else:
                suffix = ' '
            return f' {label}{suffix}'

        print('')
        for i in range(self.board_size):
            print_row(i)
        print_footer()
        print('')

    def copy_board(self):
        board_copy = Board()
        board_copy.matrix = [row[:] for row in self.matrix]
        board_copy.player_color = self.player_color
        board_copy.opponent_color = self.opponent_color
        return board_copy

    def get_cell(self, row, col):
        return self.matrix[row][col]

    def move_piece(self, move_dict):
        self.targeted_cells = []
        self.jumped_cells = []
        self.aux_cells = []
        self.selected_cell = None

        row_origin, col_origin = move_dict['origin']
        row_destination, col_destination = move_dict['destination']
        piece_to_move = self.get_cell(row_origin, col_origin)

        # promotion move flow
        if piece_to_move.islower() and row_destination in self.BOARD_BOUNDS:
            if ((piece_to_move == self.player_color[0] and
                 row_destination == self.BOARD_BOUNDS[0])
                or (piece_to_move == self.opponent_color[0] and
                    row_destination == self.BOARD_BOUNDS[1])):
                piece_to_move = piece_to_move.upper()

        # jump flow
        if move_dict['piece_to_jump'] is not False:
            row_to_jump, col_to_jump = move_dict['piece_to_jump']
            self.matrix[row_to_jump][col_to_jump] = False

        self.matrix[row_destination][col_destination] = piece_to_move
        self.matrix[row_origin][col_origin] = False

        return row_destination, col_destination

    def get_move_options(self):

        def get_move_vectors(row, col, is_player_move, is_king, is_jump):

            def get_vertical_moves(row, is_player_move, is_king, is_jump):
                vertical_moves = [-1, 1]
                if not is_king:
                    if is_player_move:
                        vertical_moves.remove(1)
                    else:
                        vertical_moves.remove(-1)

                if is_jump:
                    vertical_moves = [move * 2 for move in vertical_moves]

                for move in vertical_moves:
                    if row + move not in range(self.board_size):
                        vertical_moves.remove(move)

                if not vertical_moves:
                    return False
                return vertical_moves

            def get_horizontal_moves(col, is_jump):
                horizontal_moves = [-1, 1]
                if is_jump:
                    horizontal_moves = [move * 2 for move in horizontal_moves]

                for move in horizontal_moves:
                    if col + move not in range(self.board_size):
                        horizontal_moves.remove(move)

                if not horizontal_moves:
                    return False
                return horizontal_moves

            vertical_moves = get_vertical_moves(row, is_player_move, is_king, is_jump)
            horizontal_moves = get_horizontal_moves(col, is_jump)

            if not vertical_moves or not horizontal_moves:
                return False

            vectors = []
            for v_move in vertical_moves:
                for h_move in horizontal_moves:
                    vectors.append((v_move, h_move))

            return vectors

        def get_jump_chains(row, col, is_player_move, is_king):

            def get_jumps(row, col, is_player_move, is_king, board_copy):
                if is_player_move:
                    target = self.opponent_color[0]
                else:
                    target = self.player_color[0]

                targets = [target, target.upper()]
                vectors = get_move_vectors(row, col, is_player_move, is_king, True)
                if not vectors:
                    return False

                jumps = []
                for vector in vectors:
                    row_factor, col_factor = vector
                    row_destination = row + row_factor
                    col_destination = col + col_factor
                    row_to_jump = int((row + row_destination) / 2)
                    col_to_jump = int((col + col_destination) / 2)
                    cell_destination = board_copy.get_cell(row_destination, col_destination)
                    cell_to_jump = board_copy.get_cell(row_to_jump, col_to_jump)
                    if cell_to_jump in targets and cell_destination is False:
                        jump_dict = {
                            'origin': (row, col),
                            'piece_to_jump': (row_to_jump, col_to_jump),
                            'destination': (row_destination, col_destination),
                        }
                        jumps.append(jump_dict)

                if not jumps:
                    return False
                return jumps

            initial_jumps = get_jumps(row, col, is_player_move, is_king, self)

            if not initial_jumps:
                return False

            jump_chains = []

            for jump in initial_jumps:
                jump_chains.append([jump])

            while True:
                new_jumps_found = False
                for jump_chain in jump_chains:
                    # perform a copy of board and execute jump chain there
                    board_copy = self.copy_board()
                    for jump in jump_chain:
                        moved_piece_coordinates = board_copy.move_piece(jump)
                    # get last position of active piece and check if there's more jumps
                    new_row, new_col = moved_piece_coordinates
                    next_jumps = get_jumps(new_row, new_col, is_player_move, is_king, board_copy)
                    # append new jump chains
                    if next_jumps is not False:
                        for next_jump in next_jumps:
                            temp_jump = jump_chain
                            temp_jump.append(next_jump)
                            if temp_jump not in jump_chains:
                                jump_chains.append(temp_jump)
                            new_jumps_found = True
                    del board_copy
                if not new_jumps_found:
                    break

            max_jump_chains = get_max_chains(jump_chains)

            return max_jump_chains

        def get_moves(row, col, is_player_move, is_king):
            vectors = get_move_vectors(row, col, is_player_move, is_king, False)
            moves = []
            for vector in vectors:
                row_factor, col_factor = vector
                row_destination = row + row_factor
                col_destination = col + col_factor
                cell_destination = self.get_cell(row_destination, col_destination)
                if not cell_destination:  # aka cell is empty
                    move_dict = {
                        'origin': (row, col),
                        'piece_to_jump': False,
                        'destination': (row_destination, col_destination),
                    }
                    moves.append(move_dict)

            if not moves:
                return False
            return moves

        def get_max_chains(jump_chains):
            max_length_of_chain = max(map(len, jump_chains))
            max_chains = [chain for chain in jump_chains if len(chain) == max_length_of_chain]
            return max_chains

        if self.player_move:
            piece_code = self.player_color[0]
        else:
            piece_code = self.opponent_color[0]
        king_code = piece_code.upper()

        pieces = []
        kings = []

        for row in range(self.board_size):
            for col in range(self.board_size):
                cell = self.matrix[row][col]
                coordinates = (row, col)
                if cell == piece_code:
                    pieces.append(coordinates)
                elif cell == king_code:
                    kings.append(coordinates)

        all_pieces = pieces + kings

        if not all_pieces:  # aka no pieces aka game over
            return False

        jump_options = []
        move_options = []

        for piece in all_pieces:

            is_king = False
            if piece in kings:
                is_king = True

            jumps = get_jump_chains(piece[0], piece[1], self.player_move, is_king)
            if not jumps:
                moves = get_moves(piece[0], piece[1], self.player_move, is_king)
            else:
                for jump in jumps:
                    jump_options.append(jump)
                continue

            if not moves:
                continue
            else:
                for move in moves:
                    move_options.append(move)

        if not jump_options:
            if not move_options:  # aka all pieces blocked aka game over
                return False
            output = move_options
        else:
            output = get_max_chains(jump_options)

        return output

    def are_moves_jumps(self, move_options):
        if isinstance(move_options[0], list):
            return True
        else:
            return False

    def set_cell_decorators(self, move_options, piece_choosing_phase):
        self.targeted_cells = []
        self.jumped_cells = []
        self.aux_cells = []
        self.selected_cell = None

        jumping_obligatory = self.are_moves_jumps(move_options)
        targets = []
        jumped = []
        auxes = []
        selected = None

        if piece_choosing_phase:
            for option in move_options:
                if jumping_obligatory:
                    targets.append(option[0]['origin'])
                else:
                    targets.append(option['origin'])
        else:
            for option in move_options:
                if jumping_obligatory:
                    if len(option) > 1:
                        for aux in option[:-1]:
                            jumped.append(aux['piece_to_jump'])
                            auxes.append(aux['destination'])
                    selected = option[0]['origin']
                    targets.append(option[-1]['destination'])
                    jumped.append(option[-1]['piece_to_jump'])
                else:
                    selected = option['origin']
                    targets.append(option['destination'])

        self.targeted_cells = targets
        self.jumped_cells = jumped
        self.aux_cells = auxes
        self.selected_cell = selected

    def get_choice(self):

        def convert_cell_notation_and_check(coordinates):
            while True:
                strip = coordinates.strip()
                if len(strip) == 2 and strip[0].isalpha() and strip[1].isdigit():
                    letter, digit = strip
                    row_index = self.row_labels.index(int(digit))
                    col_index = self.col_labels.index(letter.upper())
                    if (row_index, col_index) in self.targeted_cells:
                        break
                    else:
                        coordinates = input('\nWrong choice. Choose one of >targeted< cells.\nTry again: ')
                        continue
                else:
                    coordinates = input('\nWrong input, it should be in A1 notation.\nTry again: ')
                    continue

            return (row_index, col_index)

        if self.player_move:
            choice = input('Choose one of >targeted< cells to move: ')
            selected_cell = convert_cell_notation_and_check(choice)
        else:
            selected_cell = random.choice(self.targeted_cells)

        return selected_cell

    def get_moves_for_selected(self, move_options, choice):
        jumping_obligatory = self.are_moves_jumps(move_options)
        moves_for_selected = []
        for option in move_options:
            if jumping_obligatory:
                if option[0]['origin'] == choice:
                    moves_for_selected.append(option)
            else:
                if option['origin'] == choice:
                    moves_for_selected.append(option)

        return moves_for_selected

    def select_move(self, moves_shortlist, choice):
        jumping_obligatory = self.are_moves_jumps(moves_shortlist)
        for option in moves_shortlist:
            if jumping_obligatory:
                if option[-1]['destination'] == choice:
                    return option
            else:
                if option['destination'] == choice:
                    return [option]  # formatting the move as a list for compatibility

    def toggle_player(self):
        self.player_move = not self.player_move