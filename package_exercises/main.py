# main.py

from helpers.string import shout
from helpers.math import area

num1 = 5
num2 = 8

str1 = f'the area of a {num1}-by-{num2} rectangle is {area(num1, num2)}'
print(shout(str1))
