# Zadanka 8.6

# 8.6-1

while True:
    try:
        num = int(input('Input an integer: '))
    except ValueError:
        print('Try again.')
        continue
    break

print(num)

# 8.6-2

text = input('Enter a string: ')
try:
    index = int(input('Enter an index to return from string: '))
    print(text[index - 1])
except ValueError:
    print('That was not an integer.')
except IndexError:
    print('Index out of bounds.')
