

from pathlib import Path
import csv

cwd = Path.home() / 'python.challenges' / 'practice_files'
scores_dir = cwd / 'scores.csv'
highscores_dir = cwd / 'highscores.csv'


def highscore_finder(hs_dict, row):
    '''
    Creates a dict of players (keys) with their highscores (values).
    Takes highscore board dict and a row entry from csv score log.
    '''
    if (not row['name'] in hs_dict or # if there's no player in the dict
        int(row['score']) > hs_dict[row['name']]): # or they've beat their score
        hs_dict[row['name']] = int(row['score'])
        
        
highscores = {}

with scores_dir.open(mode='r', encoding='utf-8', newline='') as file:
    reader = csv.DictReader(file)
    for row in reader:
        highscore_finder(highscores, row)

with highscores_dir.open(mode='w', encoding='utf-8', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(['name', 'highscore'])
    for player, highscore in highscores.items():
        writer.writerow([player, highscore])
