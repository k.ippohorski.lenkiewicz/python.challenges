

from pathlib import Path

docu_dir = Path.cwd() / 'practice_files' / 'documents'
image_dir = docu_dir.parent / 'images'

image_dir.mkdir(exist_ok=True)

for file in docu_dir.rglob('*.*'):
    if file.suffix in ('.png', '.gif', '.jpg'):
        file.replace(image_dir / file.name)
