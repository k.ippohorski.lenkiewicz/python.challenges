def hat(cat):
    if cat:
        return False
    else:
        return True

cats_num = 100
laps_num = 100

cats = [False] * cats_num

for lap in range(1, laps_num + 1):
    index = lap - 1
    while index < cats_num:
        cats[index] = hat(cats[index])
        index += lap

output = 'Cats with hats:'

for i in range(len(cats)):
    if cats[i]:
        output += f' {i + 1},'

print(output[:-1])
