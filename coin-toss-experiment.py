import random

def coinflip():
    '''Randomly return \'heads\' or \'tails\'.'''
    toss = random.randint(0, 1)
    if toss == 1:
        return 'heads'
    else:
        return 'tails'

trials = 10_000
tally = 0

for trial in range(trials):
    first_flip = coinflip()
    flip_counter = 1
    while True:
        next_flip = coinflip()
        flip_counter += 1
        if next_flip != first_flip:
            break
    tally += flip_counter

avg_flips = tally / trials

print(avg_flips)
