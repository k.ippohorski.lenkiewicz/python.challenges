# Zadanka 8.7

# 8.7-1

import random

def roll():
    return random.randint(1, 6)


# 8.7-2

trials = 10_000
tally = 0

for trial in range(trials):
    tally += roll()

avg = round(tally / trials)

print(avg)
