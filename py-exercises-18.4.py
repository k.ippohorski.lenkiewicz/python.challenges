# Zadanka 18.4

# 18.4-1

import tkinter as tk

window = tk.Tk()
some_text = tk.Label(text="GUIs are great!")
some_text.pack()
window.mainloop()

# 18.4-2

window = tk.Tk()
more_text = tk.Label(text="Python rocks!")
more_text.pack()
window.mainloop()

# 18.4-3

window = tk.Tk()
more_more_text = tk.Label(text="Engage")
more_more_text.pack()
window.mainloop()
