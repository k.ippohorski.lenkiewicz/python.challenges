# zadanka z rozdziału 6.2

# 6.2-1

def cube(num):
    return num**3

print(cube(5))
print(cube(8))
print(cube(75))

# 6.2-2

def greet(name):
    print(f'Hello {name}!')
    
greet('Daniel')
greet('Robert')
greet('Konrad')
