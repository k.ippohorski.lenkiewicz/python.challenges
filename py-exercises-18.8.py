# Zadanko 18.8

import tkinter as tk

def convert_temp():
    temp_f = ent_f.get()
    try:
        temp_f = int(temp_f)
    except ValueError:
        lbl_c['text'] = 'XXX'
        return False
    temp_c = (5/9) * (float(temp_f) - 32)
    lbl_c['text'] = f'{round(temp_c, 2)} \N{DEGREE CELSIUS}'
    return True

window = tk.Tk()

window.rowconfigure(0, minsize=50)
window.title('Temperature converter')
window.resizable(width=False, height=False)

# entry box fahrenheit
frm_f = tk.Frame(master=window)
ent_f = tk.Entry(master=frm_f, width=8)
lbl_f = tk.Label(master=frm_f, text='\N{DEGREE FAHRENHEIT}')

ent_f.grid(row=0, column=0, sticky='e')
lbl_f.grid(row=0, column=1, sticky='w')

# arrow button
btn = tk.Button(master=window, text='\N{RIGHTWARDS BLACK ARROW}', command=convert_temp)

# result celsius
lbl_c = tk.Label(master=window, text='___\N{DEGREE CELSIUS}')

frm_f.grid(row=0, column=0, padx=10)
btn.grid(row=0, column=1, pady=10)
lbl_c.grid(row=0, column=2, padx=10)

# added functionality: press enter instead of button
window.bind('<Return>', lambda event: convert_temp())

window.mainloop()
