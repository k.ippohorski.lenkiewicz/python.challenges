class Animal: # parent class
    can_talk = False

    def __init__(self, name, species):
        self.name = name
        self.species = species

    def __str__(self):
        return f'{self.name.capitalize()} is a {self.species}.'

    def speak(self, sound):
        return f'{self.name} says {sound}.'


# child classes below


class Pig(Animal):
    tail = 'twisted'
    
    def speak(self, sound='oink'):
        return super().speak(sound)


class Cow(Animal):
    def speak(self, sound='moo'):
        return super().speak(sound)

    def milk(self, liters):
        return f'Milking {self.name} resulted in {liters}L of milk.'


class Chicken(Animal):
    def speak(self, sound='coco'):
        return f'{self.name} cackles {sound}.'


# some farm life

pig = Pig('Dorothy', 'pig')
cow = Cow('Bertha', 'cow')
chicken = Chicken('Bob', 'chicken')

print(pig)

print(pig.speak())
print(cow.speak())
print(chicken.speak('cocococo'))

print(cow.milk(4))

