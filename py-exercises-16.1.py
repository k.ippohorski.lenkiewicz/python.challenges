# Zadanka 16.1

# 16.1-1

from urllib.request import urlopen

url = "http://olympus.realpython.org/profiles/dionysus"
page = urlopen(url)
html = page.read().decode('utf-8')

# 16.1-2

name_code = 'Name:'
color_code = 'Favorite Color:'

name_start = html.find(name_code) + len(name_code)
color_start = html.find(color_code) + len(color_code)

name = html[name_start:]
color = html[color_start:]

iters = [name, color]

for case in iters:
    i = 0
    while True:
        if case[i] == '<':
            case = case[:i]
            break
        i = i + 1
    case = case.strip()
    print(case)

# 16.1-3

import re

codes = [name_code, color_code]

for code in codes:
    pattern = f'{code}.*?<'
    match_result = re.search(pattern, html, re.DOTALL)
    extracted_text = match_result.group()
    extracted_text = re.sub(code, '', extracted_text) # trimming the code
    extracted_text = extracted_text[:-1].strip() # trimming the '<'
    print(extracted_text)
