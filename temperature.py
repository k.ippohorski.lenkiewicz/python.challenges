def convert_cel_to_far():
    temp_c = input('Enter a temperature in degrees C: ')
    conv_f = float(temp_c) * 9/5 + 32
    print(f'{temp_c} degrees C = {conv_f:.2f} degrees C')

def convert_far_to_cel():
    temp_f = input('Enter a temperature in degrees C: ')
    conv_c = (float(temp_f) - 32) * 5/9
    print(f'{temp_f} degrees C = {conv_c:.2f} degrees C')

convert_cel_to_far()
convert_far_to_cel()
    
