universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]

def enrollment_stats(university):
    '''Takes list of lists. Each individual list should contain:
      [0] - The name of university
      [1] - The total number of enrolled students
      [2] - The annual tuition fees
    Returns list of lists:
      [0] - Student enrollment values
      [1] - All annual tuition fees
    '''
    return [[i[1] for i in university], [j[2] for j in university]]

def mean(data):
    return sum(data) / len(data)

def median(data):
    data.sort()
    if len(data) % 2 == 0:
        ind = int(len(data) / 2)
        return (data[ind - 1] + data[ind]) / 2
    else:
        ind = len(data)//2
        return data[ind]

asterisks = '*' * 30

students = enrollment_stats(universities)[0]
tuitions = enrollment_stats(universities)[1]

s_total = sum(students)
t_total = sum(tuitions)
s_mean = mean(students)
t_mean = mean(tuitions)
s_median = median(students)
t_median = median(tuitions)

print(asterisks)
print()
print(f'Total students:\t  {s_total:,}')
print(f'Total tuitions:\t$ {t_total:,}')
print()
print()
print(f'Student mean:\t  {s_mean:,.2f}')
print(f'Student median:\t  {s_median:,}')
print()
print()
print(f'Tuition mean:\t$ {t_mean:,.2f}')
print(f'Tuition median:\t$ {t_median:,}')
print()
print(asterisks)
